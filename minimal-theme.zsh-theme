#!/usr/bin/env zsh

DEPENDENCES_ZSH+=( zpm-zsh/pr-is-root zpm-zsh/pr-user )
DEPENDENCES_ZSH+=( zpm-zsh/pr-return zpm-zsh/pr-exec-time zpm-zsh/pr-git zpm-zsh/pr-cwd )
DEPENDENCES_ZSH+=( zpm-zsh/pr-php zpm-zsh/pr-rust zpm-zsh/pr-node )
DEPENDENCES_ZSH+=( zpm-zsh/pr-2 )
DEPENDENCES_ZSH+=( zpm-zsh/pr-eol )
DEPENDENCES_ZSH+=( zpm-zsh/title )

if (( $+functions[zpm] )); then
  zpm                                 \
    zpm-zsh/pr-is-root,inline         \
    zpm-zsh/pr-user,inline            \
    zpm-zsh/pr-return,inline          \
    zpm-zsh/pr-exec-time,async,inline \
    zpm-zsh/pr-git,async,inline       \
    zpm-zsh/pr-cwd,inline             \
    zpm-zsh/pr-php,async,inline       \
    zpm-zsh/pr-rust,async,inline      \
    zpm-zsh/pr-node,async,inline      \
    zpm-zsh/pr-2,async,inline         \
    zpm-zsh/pr-eol,async,inline       \
    zpm-zsh/title,inline              \
    zpm-zsh/pr-zcalc,async,inline     \
    zpm-zsh/pr-correct,async,inline   \

fi

bindkey -v
bindkey -M command '^[' send-break
bindkey -M vicmd v edit-command-line
bindkey '^?' backward-delete-char
bindkey '^r' history-incremental-search-backward

export KEYTIMEOUT=1

function zle-keymap-select() {
    # Update keymap variable for the prompt
    VI_KEYMAP=$KEYMAP

    # Change cursor depending on mode.
    # Block cursor in "normal" mode, Beam in insert mode.
    [[ -n "$VI_MODE_KEEP_CURSOR" ]] || if [[ "$VI_KEYMAP" == "vicmd" ]]; then
        print -n '\e[1 q'
    else
        print -n '\e[5 q'
    fi

    zle reset-prompt
    zle -R
}

function zle-line-init() {
    zle -K viins
	export KEYTIMEOUT=1
}

zle -N zle-line-init
zle -N zle-keymap-select

setopt PROMPT_SUBST

PROMPT='%F{blue}%1~%f %# '

autoload -Uz bashcompinit && bashcompinit
